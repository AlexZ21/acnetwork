#ifndef NETWORKCLIENT_H
#define NETWORKCLIENT_H

#include "acnetwork_export.h"
#include "httpabstract.h"

#include <memory>
#include <thread>

namespace ac {

class NetworkClient
{
public:
    NetworkClient();
    ~NetworkClient();

    void sendRequest(const Request &request, const std::chrono::milliseconds &maxTimeout = std::chrono::milliseconds(10000));
    void waitForFinish();
    void stop();

    void setFollowLocation(bool followLocation);
    void setStoreBody(bool storeBody);

    void onHeaderReady(const std::function<void (const Response::Header &)> &onHeaderReadyCallback);
    void onData(const std::function<void (const char *, size_t)> &onDataCallback);
    void onReady(const std::function<void (const Response &)> &onReadyCallback);

private:
    void threadFunc(Request request, std::chrono::milliseconds maxTimeout);

private:
    std::thread m_thread;
    std::unique_ptr<HttpAbstract> m_http;

    bool m_followLocation;
    bool m_storeBody;

    std::function<void (const Response::Header &)> m_onHeaderReadyCallback;
    std::function<void (const char *, size_t)> m_onDataCallback;
    std::function<void (const Response &)> m_onReadyCallback;
};

}

#endif // NETWORKCLIENT_H
