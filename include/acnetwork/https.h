#ifndef HTTPS_H
#define HTTPS_H

#include "acnetwork_export.h"
#include "httpabstract.h"
#include "tcpsocket.h"

#include <atomic>

namespace ac {

class Https: public HttpAbstract
{
public:
    Https();

    Response sendRequest(const Request &request, const std::chrono::milliseconds &maxTimeout = std::chrono::milliseconds(10000));
    void stop();

private:
    TcpSocket m_socket;
    std::atomic_bool m_running;

};

}

#endif // HTTPS_H
