#ifndef RESPONSE_H
#define RESPONSE_H

#include "acnetwork_export.h"

#include <string>
#include <map>
#include <istream>

namespace ac {

class ACNETWORK_EXPORT Response
{
public:
    enum Status {
        Ok             = 200,
        Created        = 201,
        Accepted       = 202,
        NoContent      = 204,
        ResetContent   = 205,
        PartialContent = 206,

        MultipleChoices  = 300,
        MovedPermanently = 301,
        MovedTemporarily = 302,
        NotModified      = 304,

        BadRequest          = 400,
        Unauthorized        = 401,
        Forbidden           = 403,
        NotFound            = 404,
        RangeNotSatisfiable = 407,

        InternalServerError = 500,
        NotImplemented      = 501,
        BadGateway          = 502,
        ServiceNotAvailable = 503,
        GatewayTimeout      = 504,
        VersionNotSupported = 505,

        InvalidResponse  = 1000,
        ConnectionFailed = 1001
    };

    class Header {
    public:
        Header();
        ~Header() = default;

        Status status() const;
        std::string field(const std::string &field) const;
        uint32_t majorHttpVersion() const;
        uint32_t minorHttpVersion() const;

        void fromString(const std::string &str);

    private:
        void parseFields(std::istream &in);

    private:
        Status m_status;
        std::map<std::string, std::string> m_fields;
        uint32_t m_majorVersion;
        uint32_t m_minorVersion;
    };

    Response() = default;
    ~Response() = default;

    const Header &header() const;
    void setHeader(const Header &header);

    const std::string &body() const;
    void setBody(const std::string &body);

private:
    Header m_header;
    std::string m_body;

};

}

#endif // RESPONSE_H
