#include <acnetwork/socket.h>
#include "socketimpl.h"

#include <iostream>

namespace ac {

Socket::Socket(Type type) :
    m_type(type),
    m_socket(SocketImpl::invalidSocket()),
    m_isBlocking(true)
{

}

Socket::~Socket()
{
    close();
}

void Socket::setBlocking(bool blocking)
{
    if (m_socket != SocketImpl::invalidSocket())
        SocketImpl::setBlocking(m_socket, blocking);
    m_isBlocking = blocking;
}

bool Socket::isBlocking() const
{
    return m_isBlocking;
}

SocketHandle Socket::handle() const
{
    return m_socket;
}

void Socket::create()
{
    if (m_socket == SocketImpl::invalidSocket()) {
        SocketHandle handle = socket(PF_INET, m_type == Tcp ? SOCK_STREAM : SOCK_DGRAM, 0);
        if (handle == SocketImpl::invalidSocket()) {
            std::cerr << "Failed to create socket\n";
            return;
        }
        create(handle);
    }
}

void Socket::create(SocketHandle handle)
{
    if (m_socket == SocketImpl::invalidSocket()) {
        m_socket = handle;

        setBlocking(m_isBlocking);

        if (m_type == Tcp) {
            int yes = 1;
            if (setsockopt(m_socket, IPPROTO_TCP, TCP_NODELAY, reinterpret_cast<char*>(&yes), sizeof(yes)) == -1) {
                std::cerr << "Failed to set socket option \"TCP_NODELAY\"; all your TCP packets will be buffered\n";
            }

#ifdef SYSTEM_MACOS
            if (setsockopt(m_socket, SOL_SOCKET, SO_NOSIGPIPE, reinterpret_cast<char*>(&yes), sizeof(yes)) == -1)
                std::cerr << "Failed to set socket option \"SO_NOSIGPIPE\"\n";
#endif
        } else {
            int yes = 1;
            if (setsockopt(m_socket, SOL_SOCKET, SO_BROADCAST, reinterpret_cast<char*>(&yes), sizeof(yes)) == -1)
                std::cerr << "Failed to enable broadcast on UDP socket\n";
        }
    }
}

void Socket::close()
{
    if (m_socket != SocketImpl::invalidSocket()) {
        SocketImpl::close(m_socket);
        m_socket = SocketImpl::invalidSocket();
    }
}

}
