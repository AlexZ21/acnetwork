#include <acnetwork/https.h>
#include <acnetwork/ipaddress.h>
#include <acnetwork/socketselector.h>

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509_vfy.h>

#include <iostream>

namespace ac {

struct OpenSsl
{
    OpenSsl() {
        OpenSSL_add_all_algorithms();
        ERR_load_BIO_strings();
        ERR_load_crypto_strings();
        SSL_load_error_strings();
        isInit = SSL_library_init() >= 0;
    }

    static size_t write(SSL *ssl, const void *buf, int num) {
        return SSL_write(ssl, buf, num);
    }

    static size_t read(SSL *ssl, void *buf, int num, TcpSocket &socket,
                       std::chrono::milliseconds timeout = std::chrono::milliseconds(1000)) {
        SocketSelector selector;
        selector.add(socket);
        return selector.wait(timeout) ? SSL_read(ssl, buf, num) : -1000; // -1000 - timeout
    }

    bool isInit;
};

OpenSsl openSsl;

Https::Https() :
    m_running(false)
{
    storeBody = true;
}

Response Https::sendRequest(const Request &request, const std::chrono::milliseconds &maxTimeout)
{
    Request toSend(request);

    IpAddress host(toSend.url().host());
    uint16_t port = toSend.url().port().empty() ? 443 : std::stoi(toSend.url().port());

    if (!toSend.hasField("From"))
        toSend.setField("From", "user@acnetwork.com");

    if (!toSend.hasField("User-Agent"))
        toSend.setField("User-Agent", "acnetwork/1.x");

    if (!toSend.hasField("Host"))
        toSend.setField("Host", toSend.url().host());

    if (!toSend.hasField("Content-Length"))
        toSend.setField("Content-Length", std::to_string(toSend.body().size()));

    if ((toSend.method() == Request::Post) && !toSend.hasField("Content-Type"))
        toSend.setField("Content-Type", "application/x-www-form-urlencoded");

    if ((toSend.httpMajorVersion() * 10 + toSend.httpMinorVersion() >= 11) && !toSend.hasField("Connection"))
        toSend.setField("Connection", "close");

    Response received;

    if (!openSsl.isInit) {
        std::cerr << "OpenSSL library not initialized";
        return received;
    }

    if (m_socket.connect(host, port, std::chrono::seconds(5)) == Socket::Done) {
        // OpenSsl
        const SSL_METHOD *method = SSLv23_method();

        SSL_CTX *ctx = SSL_CTX_new(method);
        if (!ctx) {
            std::cerr << "Unable to create a new SSL context structure";
            return received;
        }

        const long flags = SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION;
        SSL_CTX_set_options(ctx, flags);

        SSL *ssl = SSL_new(ctx);
        SSL_set_fd(ssl, m_socket.handle());

        const char *const PREFERRED_CIPHERS = "HIGH:!aNULL:!kRSA:!PSK:!SRP:!MD5:!RC4";
        SSL_set_cipher_list(ssl, PREFERRED_CIPHERS);

        SSL_set_tlsext_host_name(ssl, toSend.url().host().c_str());

        int sslc = SSL_connect(ssl);
        if (sslc != 1) {
            long error = ERR_get_error();
            const char* error_str = ERR_error_string(error, NULL);
            std::cerr << "Could not build a SSL session: " << error_str;
            return received;
        }

        std::string requestStr = toSend.toString();

        if (!requestStr.empty()) {
            if (OpenSsl::write(ssl, requestStr.c_str(), requestStr.size()) > 0) {
                std::string receivedStr;
                char buffer[1024];

                bool headerReady = false;
                size_t lastHeaderFind = 0;

                int64_t msec = maxTimeout.count();
                m_running.store(true);
                while (m_running.load()) {
                    // Timeout 500ms setting to stop receiving the data
                    size_t readed = OpenSsl::read(ssl, buffer, sizeof(buffer), m_socket, std::chrono::milliseconds(500));

                    if (readed == -1000) {
                        msec -= 500;
                        if (msec <= 0)
                            m_running.store(false);
                        continue;
                    }

                    if (readed <= 0) {
                        m_running.store(false);
                        continue;
                    }

                    msec = maxTimeout.count();
                    if (readed > 0)
                        receivedStr.append(buffer, readed);

                    // Header ready check
                    if (!headerReady) {
                        size_t pos = receivedStr.find("\r\n\r\n", lastHeaderFind);
                        if (pos != std::string::npos) {
                            headerReady = true;
                            Response::Header header;
                            header.fromString(receivedStr);
                            received.setHeader(header);
                            receivedStr.erase(0, pos + 4);

                            if (onHeaderReadyCallback)
                                onHeaderReadyCallback(header);

                            if (onDataCallback && receivedStr.size() > 0)
                                onDataCallback(receivedStr.c_str(), receivedStr.size());

                        } else {
                            lastHeaderFind = receivedStr.size() - 4;
                        }

                    } else {
                        if (onDataCallback)
                            onDataCallback(buffer, readed);
                    }
                }

                m_running.store(false);

                if (storeBody)
                    received.setBody(receivedStr);
            }
        }

        m_socket.disconnect();
    }

    if (onReadyCallback)
        onReadyCallback(received);

    return received;
}

void Https::stop()
{
    m_running.store(false);
}

}
