#ifndef SOCKETIMPL_H
#define SOCKETIMPL_H

#ifdef _WIN32_WINDOWS
    #undef _WIN32_WINDOWS
#endif
#ifdef _WIN32_WINNT
    #undef _WIN32_WINNT
#endif
#define _WIN32_WINDOWS 0x0501
#define _WIN32_WINNT   0x0501

#include "../network_global.h"
#include "../socket.h"

#include <winsock2.h>
#include <ws2tcpip.h>

#include <stdint.h>

namespace ac {

class SocketImpl
{
public:
    typedef int AddrLength;

    static sockaddr_in createAddress(uint32_t address, unsigned short port);
    static SocketHandle invalidSocket();
    static void close(SocketHandle sock);
    static void setBlocking(SocketHandle sock, bool block);
    static Socket::Status getErrorStatus();

};

}

#endif // SOCKETIMPL_H
